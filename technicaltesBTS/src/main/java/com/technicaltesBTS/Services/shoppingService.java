package com.technicaltesBTS.Services;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technicaltesBTS.Models.shoppingModel;
import com.technicaltesBTS.Repositories.shoppingRepository;


@Service
@Transactional
public class shoppingService {

	@Autowired
	private shoppingRepository sr;
	
	public List<shoppingModel>ListShopping(){
		return sr.ListShopping();
	}
	public shoppingModel ListShoppingById(int id){
		return sr.ListShoppingById(id);
	}
	
	public void createnewShopping(String Name, Date createdDate) {
		sr.createnewShopping(Name, createdDate);
	}
	public void updateShopping(String Name, int id, Date createdDate) {
		sr.updateShopping(Name, id, createdDate);
	}
	public void deleteShopping(int id) {
		sr.deleteShopping(id);
	}
}
