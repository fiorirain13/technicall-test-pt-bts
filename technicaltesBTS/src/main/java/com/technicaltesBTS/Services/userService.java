package com.technicaltesBTS.Services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technicaltesBTS.Models.userModel;
import com.technicaltesBTS.Repositories.userRepository;

@Service
@Transactional
public class userService {

	@Autowired
	private userRepository ur;
	
	public List<userModel> ListUser(){
		return ur.Listuser();
	}
	
	public int idUser(String email) {
		return ur.idUser(email);
	}
	
	public String loginEmail(String email, String password) {
		return ur.loginEmail(email, password);
	}
	public String email(String email) {
		return ur.email(email);
	}
	
	public void signUp(String username, String email, String password,int phone, String address, String city, String country, String name, int postcode)
	{	ur.signUp(username, email, password, phone, address, city, country, name, postcode);
	}
}