package com.technicaltesBTS.Controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.technicaltesBTS.Models.userModel;
import com.technicaltesBTS.Services.userService;

@RestController
@RequestMapping(value ="api/user")
public class userControllerRest {
	
	@Autowired
	private userService us;
	
	@RequestMapping(value = "/")
	public List<userModel>listuser(){
		return us.ListUser();
	}
	
	@PostMapping(value = "signup")
	public String signUp(@RequestBody userModel um, HttpServletRequest request) {
		String hasil = "";
		String loginEmail = "";
		
		String namaEmail = us.email(um.getEmail());
		
		if (namaEmail != null) {
			hasil = "1";
		} else if (namaEmail == null) {
			us.signUp(um.getUsername(), um.getEmail(), um.getPassword(), 
			um.getPhone(), um.getAddress(), um.getCity(), um.getCountry(), um.getName(), um.getPostcode());
			loginEmail = us.loginEmail(um.getEmail(), um.getPassword());
			request.getSession().setAttribute("logUser", loginEmail);
			hasil = "success";
		} else {
			hasil = "0";
		}
		return hasil;
	}
	
	@PostMapping("signin")
	public String signIn(@RequestBody userModel um, HttpServletRequest request) {
		
		String hasil = "";
		String loginEmail = us.loginEmail(um.getEmail(), um.getPassword());
		System.out.println(loginEmail);
		
		if (loginEmail != null) {
			request.getSession().setAttribute("logUser", loginEmail);
			hasil = "success";
		} else {
			hasil = "failed";
		} return hasil;
	}
	

}
