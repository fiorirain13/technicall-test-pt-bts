package com.technicaltesBTS.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.technicaltesBTS.Models.shoppingModel;
import com.technicaltesBTS.Services.shoppingService;

@RestController
@RequestMapping("api/shopping")
public class shoppingControllerRest {

	@Autowired
	private shoppingService ss;
	
	@RequestMapping("listshopping")
	public List<shoppingModel> ListKota(){
        return ss.ListShopping();
    }
	
	@GetMapping(value = "listshoppingbyid/{id}")
    public shoppingModel ListShoppingById(@PathVariable int id){
        return ss.ListShoppingById(id);
    }
	
	 @PostMapping(value = "inputshopping")
	    public void creatnewShopping(@RequestBody shoppingModel sm) {
	    	ss.createnewShopping(sm.getName(), sm.getCreatedDate());	    }
	    @PutMapping(value = "updatenamashopping")
	    public void updateKota(@RequestBody shoppingModel sm) {
	    	ss.updateShopping(sm.getName(), sm.getId(), sm.getCreatedDate());
	    }
	    @DeleteMapping(value = "deleteshopingbyid/{id}")
	    public void deleteShopping(@PathVariable int id) {
	    	ss.deleteShopping(id);
	    }
}
