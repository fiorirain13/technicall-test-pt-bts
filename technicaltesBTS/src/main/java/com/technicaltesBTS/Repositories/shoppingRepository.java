package com.technicaltesBTS.Repositories;

import java.util.List;
import java.sql.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.technicaltesBTS.Models.*;

public interface shoppingRepository extends JpaRepository<shoppingModel, Integer>{

	@Query(value = "select * from x_data_shopping", nativeQuery = true)
	List<shoppingModel> ListShopping();
	
	@Query(value = "select * from x_data_shopping where id = ?1 ",nativeQuery = true)
	shoppingModel ListShoppingById(int id);
	
	@Modifying
	@Query(value = "insert into x_data_shopping (name, created_date) values (?1, ?2)", nativeQuery = true)
	int createnewShopping(String Name, Date createdDate);
	
	@Modifying
	@Query(value = "update x_data_shopping set name = ?1, created_date = ?2 where id_data_shopping = ?3 ", nativeQuery = true)
	int updateShopping(String Name, int id, Date createdDate);
	
	@Modifying
	@Query(value = "delete from x_data_shopping where id=?1", nativeQuery = true)
	int deleteShopping(int id);
}
