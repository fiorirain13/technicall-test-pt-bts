package com.technicaltesBTS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.technicaltesBTS.Models.userModel;
public interface userRepository extends JpaRepository<userModel, Integer> {

	@Query(value = "select * from x_data_user", nativeQuery = true)
	List<userModel>Listuser();
	
	@Query(value = "select id from x_data_user where email=?1", nativeQuery = true)
	int idUser(String email);
	
	@Query(value = "select email from x_data_user where email = ?1", nativeQuery = true)
	String email(String email);
	
	@Query(value = "select username from x_data_user where email=?1 and password=?2", nativeQuery = true)
	String loginEmail(String email, String  password);
	
	@Modifying
	@Query(value = "insert into x_data_user (username, email, password, phone, address, city, country, name, postcode) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", nativeQuery = true)
	int signUp(String username, String email, String password, int phone, String address, String city, String country, String name, int postcode);
}
