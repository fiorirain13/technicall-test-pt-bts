package com.technicaltesBTS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnicaltesBtsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicaltesBtsApplication.class, args);
	}

}
